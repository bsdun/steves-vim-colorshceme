# Steves vim colorscheme

Light vim colorscheme that's easy on eyes.

Installation:

Copy **steves.vim** into:

**Win:**

%USERPROFILE%\vimfiles\colors\

**+nix:**

$HOME/.vim/colors/

Add this line to your **vimrc**:

colorscheme steves

To make **vimdiff** use this colorscheme:

Add these lines to your **vimrc**:

if &diff

  colorscheme steves
  
endif
